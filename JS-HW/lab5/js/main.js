
let userName     = prompt('Введите Ваше имя', '');
let userSurname  = prompt('Введите Вашу фамилию', '');
let birthdayDate = prompt('Введи дату Вашего рождения ( дд.мм.гггг )');

function createNewUser(name, surname, date){

  let newUser = {
    firstName: name,
    lastName : surname,
    birthday : date,
    getLogin : function() {
      return this.firstName.charAt(0) + this.lastName.toLowerCase();
    },
    getAge : function(){
      let nowDate = new Date();
      let nowYear = nowDate.getFullYear();

      return nowYear - parseInt(this.birthday.slice(6,10));
    },
    getPassword: function() {
      return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6,10);
    }
  };

  return newUser.firstName + ' ' + newUser.lastName + ' ' + newUser.getAge() + ' ' + newUser.getPassword();

}

console.log(createNewUser(userName, userSurname, birthdayDate));

