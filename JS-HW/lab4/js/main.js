
let userName    = prompt('Введите Ваше имя', '');
let userSurname = prompt('Введите Вашу фамилию', '');

function createNewUser(name, surname){

  let newUser = {
    firstName: '',
    lastName : '',
    getLogin : function() {
      return console.log(this.firstName.charAt(0) + this.lastName.toLowerCase());
    }
  };

  Object.defineProperty(newUser, 'firstName', {
    value: name,
    writable: false,
    configurable: false
  });

  Object.defineProperty(newUser, 'lastName', {
    value: surname,
    writable: false,
    configurable: false
  });

  newUser.getLogin();
  return newUser.firstName + ' ' + newUser.lastName;
}

alert(createNewUser(userName, userSurname));


