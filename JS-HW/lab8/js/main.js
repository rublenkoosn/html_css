let elem = document.createElement('span');
let button = document.createElement('button');
let errorMessage = document.createElement('p');
let inputPrice = document.getElementById('price');

button.innerHTML = 'X';
errorMessage.innerHTML = 'Please enter correct price';
errorMessage.style.cssText = 'color:red;';

inputPrice.onfocus = function(){

  inputPrice.style.cssText = 'border: 2px solid green'

};

inputPrice.onblur = function(){

  inputPrice.style.cssText = 'border: 1px solid gray';

  if(Number(inputPrice.value) <= 0){
    document.body.appendChild(errorMessage);

    elem.remove();
    button.remove();
  } else if(Number(inputPrice.value) > 0){
    inputPrice.style.cssText = 'color: green;';
    elem.innerHTML = 'Текущая цена: ' + `${price.value}`;
    document.body.insertBefore(elem, inputPrice);
    elem.appendChild(button);

    errorMessage.remove();
  }

};

button.addEventListener('click', function(){
  elem.remove();

  inputPrice.value = '';
});
