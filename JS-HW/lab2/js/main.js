/**
 * Вопрос: Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
 *
 * Циклы необходимы для выполнения (обработки) большого количества однотипного кода: вывод товаров с бд, вывод модальных окон во время проверки и тд.
 **/

let userNumber = '';

while (!Number.isInteger(userNumber)) {
  userNumber = +prompt('Введите число','');

  if(userNumber == null){
    break;
  }
}

for (let i = 0; i <= userNumber; i++){
   let result = i % 5;

  if(result === 0){
    console.log('Числа кратные 5 --> ', i);
  }
}

if (userNumber < 0){
  console.log('Чисел кратных 5 нет');
}

