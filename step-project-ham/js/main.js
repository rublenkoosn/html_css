$(document).ready(function(){

  $('.our-services-menu-item').click(function(){

    const id = $(this).attr('id');

    $('.our-services-menu-item').removeClass('tabs-active');
    $(this).toggleClass('tabs-active');

    $('.our-services-tabs-content > div').hide();
    $('.our-services-tabs-content > div[data-id='+id+']').show();

  });

  $('.work-photo-container > div[data-static="static"]').show();
  $('.work-photo-container > div[data-ready="ready"]').show();

  $('.work-menu-item').click(function(){

    const idWORK = $(this).attr('id');
    // console.log(idWORK);
    $('.work-photo-container > div[data-ready="ready"]').hide();
    $('.work-photo-container > div[data-graphic='+idWORK+']').show();

  })

});


